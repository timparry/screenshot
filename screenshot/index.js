const fs = require('fs');
const CDP = require('chrome-remote-interface');
const resemble = require('node-resemble-v2');

// const WebSocket = require('ws');
 
// const ws = new WebSocket('ws://localhost:8081/');
 
// ws.on('open', function open() {
//   console.log('connected');
//   ws.send(Date.now());
// });
 
// ws.on('close', function close() {
//   console.log('disconnected');
// });




const WebSocket = require('ws');
 
const wss = new WebSocket.Server({ port: 8080 });
 
wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
 
  ws.send('something');
});


 
// ws.on('message', function incoming(data, flags) {
//   console.log(`Roundtrip time: ${Date.now() - data} ms`, flags);
 
//   setTimeout(function timeout() {
//     ws.send(Date.now());
//   }, 500);
// });


// app.get('/', (req, res) => {
//   let trackerjs = `
//     var socket = new WebSocket('ws://localhost:8080');
//     socket.onopen = function() {
//       socket.send(JSON.stringify({
//         type: 'init',
//         url: document.location.href,
//         ref: document.referrer
//       }));
//       var intervalID = setInterval(function() {
//         if (socket.readyState != socket.OPEN) return clearInterval(intervalID);
//         socket.send(JSON.stringify({
//           type: 'update',
//           scroll: 100.0*document.documentElement.scrollTop/document.documentElement.scrollHeight,
//           focus: 'hidden' in document ? !document.hidden : undefined,
//         }));
//       }, ${config.analytics.updateInterval});
//       setTimeout(function() {
//         if (socket.readyState != socket.OPEN) return;
//         socket.send(JSON.stringify({
//           type: 'timing',
//           timing: window.performance ? window.performance.timing.toJSON() : null
//         }));
//       }, ${config.analytics.timingUpdateDelay});
//     };`

//   res.set('Content-Type', 'application/javascript')
//   res.send({'poo':true})
// })







// let holdingImage;

// function compareImages(imgUrl) {
//     return new Promise(
//         function (resolve, reject) {
//             resemble(imgUrl).compareTo(holdingImage).onComplete((data)=>{
//                 let obj = Object.assign(data, {'imgUrl': imgUrl})
//                 resolve(obj);
//             })
//         });
// }

// function doPage(Page) {
//     console.log('doPage called ...')
//     let startTime = Date.now();
    
//     Page.captureScreenshot()
//     .then(v => {
//         console.log('screenshot taken')
//         let filename = `screenshot-${Date.now()}`;

//         let imgUrl = `${__dirname}/screenshots/horses/${filename}.png`
        
//         fs.writeFileSync(imgUrl, v.data, 'base64');
//         // console.log(`Image saved as ${filename}.png`);
//         let imageEnd = Date.now();
//         // console.log('image success in: ' + (+imageEnd - +startTime) + "ms");
//         return imgUrl;
//         // client.close();
//     })
//     .then((imgUrl) => {
//         if(!holdingImage) {
//             holdingImage = imgUrl;
//             return
//         } else {
//             return compareImages(imgUrl)
//         }
//     })
//     .then((data)=>{
//         if (data) {
//             console.log(data)
//             fs.unlinkSync(holdingImage);
//             holdingImage = data.imgUrl


//         } else {
//             return;
//         }
//     })
//     .then(() => {
//         setTimeout(() => {
//             doPage(Page);
//         }, 3000)
//     })
//     .catch((err) => {
//         console.error(err);
//         // client.close();
//     });
    

// }

// CDP({ port: 9222 }, client => {

//     // extract domains
//     const {Network, Page} = client;
//     console.log('starting ...')
//     Page.loadEventFired(() => doPage(Page));
//     // enable events then start!
//     Promise.all([
//         Network.enable(),
//         Page.enable()
//     ])
//     .then(() => {
//         return Page.navigate({url: 'http://localhost:8000'});
//     }).catch((err) => {
//         console.error(`ERROR: ${err.message}`);
//         client.close();
//     });
// }).on('error', (err) => {
//     console.error('Cannot connect to remote endpoint:', err);
// });